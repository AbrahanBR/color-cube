import styled from "styled-components";


export const SearchInput = styled.input`
    width: 130px;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
    transition: width 0.4s ease-in-out;

    &:focus{
        width: 100%;
    }
`