import { Color } from "../../types/color";

export const colors: Array<Color> = [
    {
        name: "red",
        value: "#ff0000"
    },
    {
        name: "green",
        value: "#008000"
    },
    {
        name: "blue",
        value: "#0000ff"
    },
]