import { Color } from "../../types/color";

export interface Props {
    onSearch?: (color:Array<Color>) =>  void;
}