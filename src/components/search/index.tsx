import React, { useEffect, useState } from "react";
import { filter, noop } from "lodash";
import { colors } from "./colors";
import { Props } from "./typesInterface";
import { SearchInput } from "./style";

const Search = (props: Props) => {
    const {onSearch = noop} = props;
 
    let [colorPiker, setColorPiker] = useState("");
    let [colorInput, setColorInput] = useState("");
    useEffect(() => {
        const filteredColor = filter(colors, (color => color.name.toLocaleLowerCase().includes(colorInput.toLocaleLowerCase())));
        onSearch(filteredColor);
        console.log(filteredColor)
    },[colorInput]);

    return (
        <>
            <h1 style={{color: colorPiker}}>Color: {colorPiker}</h1>
            <div style={{width:60, height:60, backgroundColor: colorPiker}}></div>
            <SearchInput type="text" value={colorInput} onChange={(e) => {setColorInput(e.target.value)}}/>  
            <input type="color" value={colorPiker} onChange={(e) => {setColorPiker(e.target.value)}}/>  
        </>
    )
}


export default Search;