import styled from "styled-components";
interface Color{
    color: string;
}

const recevedColor = ({color}:Color) => color;

export const HeaderStyled = styled.header<Color>`
    text-align: center;
    padding: 5px;
    background-color: ${recevedColor};
    color: #fff;
`;

export const H1Styled = styled.h1<Color>`
    color: ${recevedColor};
    filter: invert(100%);
    text-shadow: 1px 1px 2px #fff;
`;