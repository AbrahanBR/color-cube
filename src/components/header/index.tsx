import React, { useMemo } from "react";
import { getRandonColor } from "../../utils/randonmize-numbers";
import { H1Styled, HeaderStyled } from "./style";

const color = getRandonColor();

const Header = () => (
    
    <HeaderStyled color={color}>
        <H1Styled color={color}>Color Cube</H1Styled>
    </HeaderStyled>
)

export default Header;