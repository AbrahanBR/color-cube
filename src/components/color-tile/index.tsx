import React, { useState } from "react";
import { Props } from "./types";
import { Tile } from "./style";

const renderDetails = (name:string, value:string) =>{ 
    
    return(
        <>
            <h1>{name}</h1>
            <p>{value}</p>
        </>
)}

const ColorTile = (props: Props) => {
    const [showDetails, setShowDetails] = useState(false);
    const { name, value } = props;
    return (
        <>
            <Tile bgColor={value}
                onMouseEnter={() => setShowDetails(true)}
                onMouseLeave={() => setShowDetails(false)}
            >
            {showDetails && renderDetails(name,value)}
                
            </Tile>
        </>
    )
}

export default ColorTile;