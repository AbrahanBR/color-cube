import styled from "styled-components";

interface Color{
   bgColor: string;
}

const bgColor = ({bgColor}:Color) => bgColor; 

export const Tile = styled.div<Color>`
   width: 130px;
   height: 130px;
   border: 1px solid #c7c7c7;
   background-color: ${bgColor};

   &:hover {
      cursor: pointer;
   }
`