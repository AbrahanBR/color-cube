import React, { useEffect, useState }from "react";
import ColorTile from "./components/color-tile";
import HeaderComp from "./components/header";
import Search from "./components/search";
import { ColorWrap } from "./style";
import { Color } from "./types/color";

const App = () => {
  const [colors, setcolors] = useState<Array<Color>>([])
  return (
    <>
      <HeaderComp/>
      <Search onSearch={(colors) => setcolors(colors)}/>
      <ColorWrap>
        {colors.map((color) => <ColorTile {...color} key={color.name} />)}
      </ColorWrap>
    </>
  )
}

export default App;  